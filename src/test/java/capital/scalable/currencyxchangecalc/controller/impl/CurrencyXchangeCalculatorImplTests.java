package capital.scalable.currencyxchangecalc.controller.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Map;
import org.junit.*;
import org.junit.jupiter.api.Test;
import org.junit.runner.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import capital.scalable.currencyxchangecalc.TestUtils;
import capital.scalable.currencyxchangecalc.controller.helper.CurrencyXchangeCalculatorHelper;
import capital.scalable.currencyxchangecalc.controller.util.Constants;
import capital.scalable.currencyxchangecalc.controller.util.CurrencyXchangeValidator;

@RunWith(SpringRunner.class)
@SpringBootTest
class CurrencyXchangeCalculatorImplTests {
    
    public String CHART_LINK= "https://www.xe.com/currencycharts/?from=EUR&to=INR";
    public String EUR = "EUR";
    public String INR = "INR";
    public String GBP = "GBP";
    public String USD = "USD";
    public String AMOUNT = "1000";
    
    @InjectMocks
    CurrencyXchangeCalculatorImpl currencyXchangeCalculatorImpl = new CurrencyXchangeCalculatorImpl();
    
    @Mock
    CurrencyXchangeCalculatorHelper currencyXchangeCalculatorHelper;
    
    @Mock 
    CurrencyXchangeValidator currencyXchangeValidator;
    
    @Mock 
    Constants constants;
    
    @Test
    public void testGetReferenceRate() {

        when(currencyXchangeCalculatorImpl.getReferenceRate(EUR, INR)).thenReturn(new BigDecimal("88.3307"));
        when(constants.getUSED_FOR_REFERENCE_RATE()).thenReturn("forReferenceRate");
        BigDecimal output = currencyXchangeCalculatorImpl.getReferenceRate(EUR, INR);
        assertEquals(new BigDecimal("88.3307"), output);
        verify(currencyXchangeValidator, times(1)).validateCurrencies("forReferenceRate", "EUR", "INR");
    }
    
    @Test
    public void testGetXchangeRate() {
        
      when(currencyXchangeCalculatorImpl.getXchangeRate(GBP, USD)).thenReturn(new BigDecimal("1.33"));
      when(constants.getUSED_FOR_REFERENCE_RATE()).thenReturn("getXchangeRate");
      BigDecimal output = currencyXchangeCalculatorImpl.getXchangeRate(GBP, USD);
      assertEquals(new BigDecimal("1.33"), output);
    }
    
    @Test
    public void testGetCurrenciesAndTimesRequestedList() {
        when(currencyXchangeCalculatorImpl.getSupportedCurrenciesWithTimeRequested()).thenReturn(TestUtils.getCurrenciesRequested());
        Map<String, Integer> output = currencyXchangeCalculatorImpl.getSupportedCurrenciesWithTimeRequested();
        assertEquals(TestUtils.getCurrenciesRequested(), output);
    }
    
    @Test
    public void testConvertAmount() {
        when(currencyXchangeCalculatorImpl.convertAmount(AMOUNT, GBP, INR)).thenReturn(new BigDecimal("98625.77"));
        BigDecimal output = currencyXchangeCalculatorImpl.convertAmount(AMOUNT, GBP, INR);
        assertEquals(new BigDecimal("98625.77"), output);
    }
    
    @Test
    public void testGetChartLink() {
        when(currencyXchangeCalculatorImpl.getChartLink(EUR, INR)).thenReturn(CHART_LINK);
        String output = currencyXchangeCalculatorImpl.getChartLink(EUR, INR);
        assertEquals(CHART_LINK, output);
    }

}