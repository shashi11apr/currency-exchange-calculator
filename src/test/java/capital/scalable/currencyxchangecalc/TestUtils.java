package capital.scalable.currencyxchangecalc;

import java.util.HashMap;
import java.util.Map;

public class TestUtils {

    public static Map<String, Integer> getCurrenciesRequested() {

        Map<String, Integer> currenciesRequested = new HashMap<String, Integer>();
        
        currenciesRequested.put("EUR", 0);
        currenciesRequested.put("INR", 3);
        currenciesRequested.put("GBP", 1);
        currenciesRequested.put("USD", 0);
        
        return currenciesRequested;
    }

}
