package capital.scalable.currencyxchangecalc.controller.util;

import org.springframework.stereotype.Component;

/**
 * Exchange Rate Service Constants
 */
@Component
public class Constants {
	
    public final String DEFAULT_CURRENCY = "EUR";
    public final String DEFAULT_CURRENCY_AMOUNT = "1";
    public final String EMPTY_STRING = "";
    public final String CHECK_CURRENCY_REGEX = "^[A-Z]{3}";
    public final String ECB_URL = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml?ff45ae33866b45264c557233c2e88c10";
    public final String CHART_URL = "https://www.xe.com/currencycharts/?from=%s&to=%s";
    public final String CURRENCY_XPATH = "//Cube/Cube/Cube";
    public final String CURRENCY_XML_ATTRIBUTE = "currency";
    public final String RATE_XML_ATTRIBUTE = "rate";
	public final String INTERNAL_SERVER_ERROR = "Internal Server Error";
	public final String USED_FOR_REFERENCE_RATE = "forReferenceRate";
	public final String USED_FOR_XCHANGE_RATE = "forXchangeRate";
	public final String CALCULATING_REF_RATE = "Calculating reference rate of %s and %s";
	public final String CALCULATED_REF_RATE = "Calculated reference rate of %s and %s";
	public final String CALCULATING_EXCHANGE_RATE = "Calculating Exchange rate of %s and %s";
	public final String CALCULATED_EXCHANGE_RATE = "Calculated Exchange rate of %s and %s";
	public final String CURRENCIES_AND_REQUESTED_TIME = "Calculated supported currencies with number of times requested.";
	public final String CONVERTING_AMOUNT = "Converting an amount %s from %s to %s";
	public final String CONVERTED_AMOUNT = "Converted an amount %s from %s to %s";
	public final String INTERACTIVE_LINK = "Returned the link to view interactive chart of %s and %s rates";
	public final String INPUT_VALIDATION = "Validating input parameters %s and %s";
	public final String INPUT_VALIDATION_WITH_AMOUNT = "Validating input parameters %s, %s and %S";
	public final String VALIDATION_FAILED = "Validation Failed";
    /**
     * @return the dEFAULT_CURRENCY
     */
    public String getDEFAULT_CURRENCY() {
        return DEFAULT_CURRENCY;
    }
    /**
     * @return the dEFAULT_CURRENCY_AMOUNT
     */
    public String getDEFAULT_CURRENCY_AMOUNT() {
        return DEFAULT_CURRENCY_AMOUNT;
    }
    /**
     * @return the eMPTY_STRING
     */
    public String getEMPTY_STRING() {
        return EMPTY_STRING;
    }
    /**
     * @return the cHECK_CURRENCY_REGEX
     */
    public String getCHECK_CURRENCY_REGEX() {
        return CHECK_CURRENCY_REGEX;
    }
    /**
     * @return the eCB_URL
     */
    public String getECB_URL() {
        return ECB_URL;
    }
    /**
     * @return the cHART_URL
     */
    public String getCHART_URL() {
        return CHART_URL;
    }
    /**
     * @return the cURRENCY_XPATH
     */
    public String getCURRENCY_XPATH() {
        return CURRENCY_XPATH;
    }
    /**
     * @return the cURRENCY_XML_ATTRIBUTE
     */
    public String getCURRENCY_XML_ATTRIBUTE() {
        return CURRENCY_XML_ATTRIBUTE;
    }
    /**
     * @return the rATE_XML_ATTRIBUTE
     */
    public String getRATE_XML_ATTRIBUTE() {
        return RATE_XML_ATTRIBUTE;
    }
    /**
     * @return the iNTERNAL_SERVER_ERROR
     */
    public String getINTERNAL_SERVER_ERROR() {
        return INTERNAL_SERVER_ERROR;
    }
    /**
     * @return the uSED_FOR_REFERENCE_RATE
     */
    public String getUSED_FOR_REFERENCE_RATE() {
        return USED_FOR_REFERENCE_RATE;
    }
    /**
     * @return the uSED_FOR_XCHANGE_RATE
     */
    public String getUSED_FOR_XCHANGE_RATE() {
        return USED_FOR_XCHANGE_RATE;
    }
    /**
     * @return the cALCULATING_REF_RATE
     */
    public String getCALCULATING_REF_RATE() {
        return CALCULATING_REF_RATE;
    }
    /**
     * @return the cALCULATED_REF_RATE
     */
    public String getCALCULATED_REF_RATE() {
        return CALCULATED_REF_RATE;
    }
    /**
     * @return the cALCULATING_EXCHANGE_RATE
     */
    public String getCALCULATING_EXCHANGE_RATE() {
        return CALCULATING_EXCHANGE_RATE;
    }
    /**
     * @return the cALCULATED_EXCHANGE_RATE
     */
    public String getCALCULATED_EXCHANGE_RATE() {
        return CALCULATED_EXCHANGE_RATE;
    }
    /**
     * @return the cURRENCIES_AND_REQUESTED_TIME
     */
    public String getCURRENCIES_AND_REQUESTED_TIME() {
        return CURRENCIES_AND_REQUESTED_TIME;
    }
    /**
     * @return the cONVERTING_AMOUNT
     */
    public String getCONVERTING_AMOUNT() {
        return CONVERTING_AMOUNT;
    }
    /**
     * @return the cONVERTED_AMOUNT
     */
    public String getCONVERTED_AMOUNT() {
        return CONVERTED_AMOUNT;
    }
    /**
     * @return the iNTERACTIVE_LINK
     */
    public String getINTERACTIVE_LINK() {
        return INTERACTIVE_LINK;
    }
    /**
     * @return the iNPUT_VALIDATION
     */
    public String getINPUT_VALIDATION() {
        return INPUT_VALIDATION;
    }
    /**
     * @return the iNPUT_VALIDATION_WITH_AMOUNT
     */
    public String getINPUT_VALIDATION_WITH_AMOUNT() {
        return INPUT_VALIDATION_WITH_AMOUNT;
    }
    /**
     * @return the vALIDATION_FAILED
     */
    public String getVALIDATION_FAILED() {
        return VALIDATION_FAILED;
    }
	
	
}
