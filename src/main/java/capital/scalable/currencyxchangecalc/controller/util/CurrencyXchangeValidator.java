
package capital.scalable.currencyxchangecalc.controller.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import capital.scalable.currencyxchangecalc.CurrencyXchangeCalcApplication;
import capital.scalable.currencyxchangecalc.controller.exception.ConstraintViolationException;
import capital.scalable.currencyxchangecalc.controller.exception.ErrorMessageKey;

/**
 * Validator for input parameters
 */
@Component
public class CurrencyXchangeValidator {

	private static final Logger logger = LoggerFactory.getLogger(CurrencyXchangeValidator.class);
	
	@Autowired
	Constants constants;
	
    public void validateCurrencies(String usedFor, String sourceCurrency, String destCurrency) {

    	logger.info(String.format(constants.getINPUT_VALIDATION(), sourceCurrency, destCurrency));
        List<ErrorMessageKey> errors = new ArrayList<>();
        validateCurrencies(usedFor, sourceCurrency, destCurrency, errors);
        
        if (!errors.isEmpty()) {
        	logger.error(constants.getVALIDATION_FAILED());
            throw new ConstraintViolationException(errors);
        }
    }

    public void validateAmountAndCurrencies(String sourceAmount, String sourceCurrency, String destCurrency) {
        
    	logger.info(String.format(constants.getINPUT_VALIDATION_WITH_AMOUNT(), sourceAmount, sourceCurrency, destCurrency));
        List<ErrorMessageKey> errors = new ArrayList<>();

        isNull(sourceAmount, ErrorMessageKey.SOURCE_AMOUNT_MISSING, errors);
        isValidSourceAmount(sourceAmount, ErrorMessageKey.SOURCE_AMOUNT_INVALID, errors);
        validateCurrencies(null, sourceCurrency, destCurrency, errors);
        
        if (!errors.isEmpty()) {
        	logger.error(constants.getVALIDATION_FAILED());
            throw new ConstraintViolationException(errors);
        }
    }

    private void validateCurrencies(String usedFor, String sourceCurrency, String destCurrency, List<ErrorMessageKey> errors) {

    	if (null != usedFor && constants.getUSED_FOR_REFERENCE_RATE().equals(usedFor)) {
    		isCurrenciesContainsEur(sourceCurrency, destCurrency, ErrorMessageKey.REFERENCE_RATE_SHOULD_CONTAIN_EUR, errors);
		}
    	else if (null != usedFor && constants.getUSED_FOR_XCHANGE_RATE().equals(usedFor)) {
			checkCurrenciesContainsEur(sourceCurrency, destCurrency, ErrorMessageKey.EXCHANGE_RATE_SHOULD_NOT_CONTAIN_EUR, errors);
		}
        isNull(sourceCurrency, ErrorMessageKey.SOURCE_CURRENCY_MISSING, errors);
        isValidCurrencyCode(sourceCurrency, ErrorMessageKey.SOURCE_CURRENCY_INVALID, errors);

        isNull(destCurrency, ErrorMessageKey.DESTINATION_CURRENCY_MISSING, errors);
        isValidCurrencyCode(destCurrency, ErrorMessageKey.DESTINATION_CURRENCY_INVALID, errors);
        
        isCurrencyPresentInEcb(sourceCurrency, ErrorMessageKey.SOURCE_CURRENCY_NOT_FOUND_IN_ECB, errors);
        isCurrencyPresentInEcb(destCurrency, ErrorMessageKey.DESTINATION_CURRENCY_NOT_FOUND_IN_ECB, errors);
    }

	private void isCurrencyPresentInEcb(String currency, ErrorMessageKey errorMessage, List<ErrorMessageKey> errors) {

    	if (!constants.getDEFAULT_CURRENCY().equals(currency) && !CurrencyXchangeCalcApplication.ecbCurrencyList.containsKey(currency)) {
        	errors.add(errorMessage);
        }
	}

	private void isNull(Object obj, ErrorMessageKey errorMessageKey, List<ErrorMessageKey> errors) {
        if (constants.getEMPTY_STRING().equals(obj) || null == obj) {
            errors.add(errorMessageKey);
        }
    }

    private void isValidCurrencyCode(String sourceCurrencyCode, ErrorMessageKey errorMessageKey, List<ErrorMessageKey> errors) {
        if (!constants.getEMPTY_STRING().equals(sourceCurrencyCode)) {
            if (!sourceCurrencyCode.matches(constants.getCHECK_CURRENCY_REGEX())) {
                errors.add(errorMessageKey);
            }
        }
    }

    private void isValidSourceAmount(String sourceAmount, ErrorMessageKey errorMessageKey, List<ErrorMessageKey> errors) {
        if (!constants.getEMPTY_STRING().equals(sourceAmount)) {
            try {
                BigDecimal sourceAsNumber = new BigDecimal(sourceAmount);
                isValid(sourceAmount, sourceAsNumber.compareTo(BigDecimal.ZERO) >= 0, errorMessageKey, errors);
            } catch (NumberFormatException e) {
                errors.add(errorMessageKey);
            }
        }
    }

    private void isValid(Object obj, boolean isValid, ErrorMessageKey errorMessageKey, List<ErrorMessageKey> errors) {
        if (null != obj && !isValid) {
            errors.add(errorMessageKey);
        }
    }

	public void isCurrenciesContainsEur(String sourceCurrency, String destCurrency, ErrorMessageKey errorMessageKey, List<ErrorMessageKey> errors) {

		if (!constants.getDEFAULT_CURRENCY().equals(sourceCurrency) && !constants.getDEFAULT_CURRENCY().equals(destCurrency)) {
			errors.add(errorMessageKey);
		}
	}
	
	private void checkCurrenciesContainsEur(String sourceCurrency, String destCurrency, ErrorMessageKey errorMessageKey, List<ErrorMessageKey> errors) {

		if (constants.getDEFAULT_CURRENCY().equals(sourceCurrency) || constants.getDEFAULT_CURRENCY().equals(destCurrency)) {
			errors.add(errorMessageKey);
		}
	}
}


