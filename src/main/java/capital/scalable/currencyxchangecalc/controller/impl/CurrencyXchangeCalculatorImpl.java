package capital.scalable.currencyxchangecalc.controller.impl;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import capital.scalable.currencyxchangecalc.controller.ICurrencyXchangeCalculator;
import capital.scalable.currencyxchangecalc.controller.helper.CurrencyXchangeCalculatorHelper;
import capital.scalable.currencyxchangecalc.controller.util.Constants;
import capital.scalable.currencyxchangecalc.controller.util.CurrencyXchangeValidator;

/**
 * Implementation for all requested APIs
 */
@RestController
public class CurrencyXchangeCalculatorImpl implements ICurrencyXchangeCalculator{

    @Autowired
    CurrencyXchangeCalculatorHelper currencyXchangeCalculatorHelper;
    
    @Autowired
    CurrencyXchangeValidator currencyXchangeValidator;
    
    @Autowired
    Constants constants;
    
    /**
     * getReferenceRate - calculate the reference rate for the input currencies.
     * 
     * @param sourceCurrency - source currency
     * @param destCurrency - destination currency
     * @return {@link BigDecimal} of the calculated reference rate
     */
    @Override
    public BigDecimal getReferenceRate(String sourceCurrency, String destCurrency) {
        
        currencyXchangeValidator.validateCurrencies(constants.getUSED_FOR_REFERENCE_RATE(), sourceCurrency, destCurrency);
        return currencyXchangeCalculatorHelper.getReferenceRate(sourceCurrency, destCurrency);
    }

    
    /**
     * getXchangeRate - calculate the exchange rate for the input currencies.
     * 
     * @param sourceCurrency - source currency
     * @param destCurrency - destination currency
     * @return {@link BigDecimal} of the calculated exchange rate
     */
    @Override
	public BigDecimal getXchangeRate(String sourceCurrency, String destCurrency) {
        
        System.out.println("sourceCurrency: "+sourceCurrency+", destCurrency: "+destCurrency);
		
		currencyXchangeValidator.validateCurrencies(constants.getUSED_FOR_XCHANGE_RATE(), sourceCurrency, destCurrency);
		return currencyXchangeCalculatorHelper.getXchangeRate(sourceCurrency, destCurrency);
	}
    
    /**
     *  getSupportedCurrenciesWithTimeRequested - retrieve a list of supported currencies and see how many times they were requested.  
     * 
     * @return {@link Map<String, Integer>} of the supported currencies and number of times they were requested.
     */
    @Override
    public Map<String, Integer> getSupportedCurrenciesWithTimeRequested() {

        Map<String, Integer> currencyMap = currencyXchangeCalculatorHelper.getCurrenciesAndTimesRequestedList();
        return currencyMap;
    }
    
    /**
     * convertAmount - convert an amount in a given currency to another, e.g. 15 EUR = ??? GBP 
     * 
     * @param sourceAmount - Source Currency
     * @param sourceCurrency - Source Currency
     * @param destCurrency - Destination Currency
     * @return {@link BigDecimal} of the converted amount.
     */
    @Override
    public BigDecimal convertAmount(String sourceAmount, String sourceCurrency, String destCurrency) {
        
        currencyXchangeValidator.validateAmountAndCurrencies(sourceAmount, sourceCurrency, destCurrency);
        return currencyXchangeCalculatorHelper.convertAmount(sourceAmount, sourceCurrency, destCurrency);
    }
    
    /**
     * getChartLink - retrieve a link to a public website showing an interactive chart for a given currency pair.
     * 
     * @param sourceCurrency - Source Currency
     * @param destCurrency - Destination Currency
     * @return {@link String} of the link to the public website.
     */
    @Override
	public String getChartLink(String sourceCurrency, String destCurrency) {
        
        currencyXchangeValidator.validateCurrencies(null, sourceCurrency, destCurrency);
		return currencyXchangeCalculatorHelper.getLink(sourceCurrency, destCurrency);
	}
}
