package capital.scalable.currencyxchangecalc.controller;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Exchange Rate Service APIs.
 */
@RestController
@RequestMapping("/currency-xchange")
public interface ICurrencyXchangeCalculator {
    
    /**
     * Calculate the reference rate of given pair of currencies.
     * Retrieve the ECB reference rate for a currency pair, e.g. USD/EUR or HUF/EUR
     * 
     * @param sourceCurrency - Source Currency
     * @param destCurrency - Destination Currency
     * @return {@link BigDecimal} of the calculated reference rate
     */
    @GetMapping("/getReferenceRate")
    public BigDecimal getReferenceRate(@RequestParam String sourceCurrency, @RequestParam String destCurrency);
    
    /**
     * Calculate the exchange rate of given pair of currencies.
     * Retrieve an exchange rate for other pairs, e.g. HUF/USD. 
     * 
     * @param sourceCurrency - Source Currency
     * @param destCurrency - Destination Currency
     * @return {@link BigDecimal} of the calculated exchange rate.
     */
    @GetMapping("/getXchangeRate")
    public BigDecimal getXchangeRate(@RequestParam String sourceCurrency, @RequestParam String destCurrency);

    /**
     *  Retrieve a list of supported currencies and see how many times they were requested.  
     * 
     * @return {@link Map<String, Integer>} of the supported currencies and number of times they were requested.
     */
    @GetMapping("/getSupportedCurrenciesWithTimeRequested")
    public Map<String, Integer> getSupportedCurrenciesWithTimeRequested();

    /**
     * Convert an amount in a given currency to another, e.g. 15 EUR = ??? GBP 
     * 
     * @param sourceAmount - Source Currency
     * @param sourceCurrency - Source Currency
     * @param destCurrency - Destination Currency
     * @return {@link BigDecimal} of the converted amount.
     */
    @GetMapping("/convertAmount")
    public BigDecimal convertAmount(@RequestParam String sourceAmount, @RequestParam String sourceCurrency, @RequestParam String destCurrency);

    /**
     * Retrieve a link to a public website showing an interactive chart for a given currency pair.
     * 
     * @param sourceCurrency - Source Currency
     * @param destCurrency - Destination Currency
     * @return {@link String} of the link to the public website.
     */
    @GetMapping("/getChartLink")
    public String getChartLink(@RequestParam String sourceCurrency, @RequestParam String destCurrency);
}
