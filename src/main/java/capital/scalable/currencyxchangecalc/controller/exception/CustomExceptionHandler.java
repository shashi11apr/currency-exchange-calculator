package capital.scalable.currencyxchangecalc.controller.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

/**
 * Exception Handler
 */
@ControllerAdvice
public class CustomExceptionHandler {

	@ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ConstraintViolationException> handleConstraintViolationException(ConstraintViolationException errors, WebRequest request) {
	    
	    List<ErrorMessageKey> errorMessages = new ArrayList<>();
	    for (int i = 0; i < errors.getErrors().size(); i++) {
            
	        errorMessages.add(errors.getErrors().get(i));
        }
        ErrorDeails errorDeails = new ErrorDeails(new Date(), errorMessages, request.getDescription(false));
        
        return new ResponseEntity(errorDeails, HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler(Exception.class)
    public ResponseEntity<Exception> handleGlobalException(Exception exception, WebRequest request) {
	    
	    List<ErrorMessageKey> errorMessages = new ArrayList<>();
	    errorMessages.add(ErrorMessageKey.INTERNAL_SERVER_ERROR);
        ErrorDeails errorDeails = new ErrorDeails(new Date(), errorMessages, request.getDescription(false));
        
        return new ResponseEntity(errorDeails, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
