package capital.scalable.currencyxchangecalc.controller.exception;

import java.util.List;

/**
 * Exception for ConstraintViolations
 */
@SuppressWarnings("serial")
public class ConstraintViolationException extends RuntimeException {
    private List<ErrorMessageKey> errors;

    /**
     * Build the exception with the list of {@link ErrorMessageKey}
     * 
     * @param messageKeys error messages
     */
    public ConstraintViolationException(List<ErrorMessageKey> messageKeys) {
        super();
        this.errors = messageKeys;
    }

    /**
     * @return the errors
     */
    public List<ErrorMessageKey> getErrors() {
        return errors;
    }
}
