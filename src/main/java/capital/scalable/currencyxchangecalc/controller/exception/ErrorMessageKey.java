package capital.scalable.currencyxchangecalc.controller.exception;

/**
 * Message key for errors
 * <ul>
 * <li>SOURCE_AMOUNT_MISSING - Source amount is missing</li>
 * <li>SOURCE_AMOUNT_INVALID - Source amount is invalid</li>
 * <li>SOURCE_CURRENCY_MISSING - Source currency is missing</li>
 * <li>SOURCE_CURRENCY_INVALID - Source currency is invalid</li>
 * <li>DESTINATION_CURRENCY_MISSING - Destination currency is missing</li>
 * <li>DESTINATION_CURRENCY_INVALID - Destination currency code is invalid</li>
 * <li>SOURCE_CURRENCY_NOT_FOUND_IN_ECB - SOURCE CURRENCY NOT FOUND IN ECB</li>
 * <li>DESTINATION_CURRENCY_NOT_FOUND_IN_ECB - DESTINATION CURRENCY NOT FOUND IN ECB</li>
 * <li>INTERNAL_SERVER_ERROR - Internal server error occurred</li>
 * <li>REFERENCE_RATE_SHOULD_CONTAIN_EUR - Reference rate should contain EUR</li>
 * <li>EXCHANGE_RATE_SHOULD_NOT_CONTAIN_EUR - Exchange rate should not contain EUR</li>
 * </ul>
 */
public enum ErrorMessageKey {
    //@formatter:off
    SOURCE_AMOUNT_MISSING("Source amount is missing"), 
    SOURCE_AMOUNT_INVALID("Source amount is invalid"), 
    SOURCE_CURRENCY_MISSING("Source currency is missing"), 
    SOURCE_CURRENCY_INVALID("Source currency is invalid"), 
    DESTINATION_CURRENCY_MISSING("Destination currency is missing"), 
    DESTINATION_CURRENCY_INVALID("Destination currency code is invalid"), 
	SOURCE_CURRENCY_NOT_FOUND_IN_ECB("Source currency is not found in ECB"),
	DESTINATION_CURRENCY_NOT_FOUND_IN_ECB("Destination currency not found in ECB"),
    INTERNAL_SERVER_ERROR("Internal server error occurred"),
	REFERENCE_RATE_SHOULD_CONTAIN_EUR("Reference rate should contain EUR"),
	EXCHANGE_RATE_SHOULD_NOT_CONTAIN_EUR("Exchange rate should not contain EUR");
    //@formatter:on

    private String description;

    private ErrorMessageKey(String description) {
        this.description = description;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
}
