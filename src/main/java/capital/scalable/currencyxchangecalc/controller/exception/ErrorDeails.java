package capital.scalable.currencyxchangecalc.controller.exception;

import java.util.Date;
import java.util.List;

/**
 * Error details for validations and exceptions
 */
public class ErrorDeails {

    private Date time;
    private List<ErrorMessageKey> message;
    private String details;

    public ErrorDeails(Date time, List<ErrorMessageKey> message, String details) {
        super();
        this.time = time;
        this.message = message;
        this.details = details;
    }

    public Date getTimeStamp() {
        return time;
    }

    public void setTimeStamp(Date time) {
        this.time = time;
    }

    public List<ErrorMessageKey> getMessage() {
        return message;
    }

    public void setMessage(List<ErrorMessageKey> message) {
        this.message = message;
    }

    public String getPath() {
        return details;
    }

    public void setPath(String details) {
        this.details = details;
    }


}

