package capital.scalable.currencyxchangecalc.controller.helper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import capital.scalable.currencyxchangecalc.CurrencyXchangeCalcApplication;
import capital.scalable.currencyxchangecalc.controller.util.Constants;

/**
 * Implementation Helper for all requested APIs
 */
@Component
public class CurrencyXchangeCalculatorHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(CurrencyXchangeCalculatorHelper.class);
	
	@Autowired
	Constants constants;
	
	public BigDecimal getReferenceRate(String sourceCurrency, String destCurrency) {

	    System.out.println(sourceCurrency);
	    System.out.println(destCurrency);
	    
		logger.info(String.format(constants.getCALCULATING_REF_RATE(), sourceCurrency, destCurrency));
		updateRequestedCurrencyCount(sourceCurrency, destCurrency);
        Map<String, String> rates = CurrencyXchangeCalcApplication.ecbCurrencyList;
        
        if (constants.getDEFAULT_CURRENCY().equals(sourceCurrency)) {
        	logger.info(String.format(constants.getCALCULATED_REF_RATE(), sourceCurrency, destCurrency));
            return new BigDecimal(rates.get(destCurrency));
        } else {
        	logger.info(String.format(constants.getCALCULATED_REF_RATE(), sourceCurrency, destCurrency));
            return new BigDecimal(constants.getDEFAULT_CURRENCY_AMOUNT()).divide(new BigDecimal(rates.get(sourceCurrency)), 5, RoundingMode.CEILING);
        }
    }
	
	public BigDecimal getXchangeRate(String sourceCurrency, String destCurrency) {

		logger.info(String.format(constants.getCALCULATING_EXCHANGE_RATE(), sourceCurrency, destCurrency));
		updateRequestedCurrencyCount(sourceCurrency, destCurrency);
		logger.info(String.format(constants.getCALCULATED_EXCHANGE_RATE(), sourceCurrency, destCurrency));
		return calculateXchangeRate(null, sourceCurrency, destCurrency);
	}
	
	public Map<String, Integer> getCurrenciesAndTimesRequestedList() {
		logger.info(constants.getCURRENCIES_AND_REQUESTED_TIME());
		return CurrencyXchangeCalcApplication.currencyRequested;
	}
	
	public BigDecimal convertAmount(String sourceAmount, String sourceCurrency, String destCurrency) {
		logger.info(String.format(constants.getCONVERTING_AMOUNT(), sourceAmount, sourceCurrency, destCurrency));
		updateRequestedCurrencyCount(sourceCurrency, destCurrency);

        Map<String, String> rates = CurrencyXchangeCalcApplication.ecbCurrencyList;
        BigDecimal sourceAmt = new BigDecimal(sourceAmount);
        BigDecimal convertedAmount = null;

        if (constants.getDEFAULT_CURRENCY().equals(sourceCurrency)) {

            BigDecimal destCurr = new BigDecimal(rates.get(destCurrency));
            convertedAmount = sourceAmt.multiply(destCurr);
        } else if (constants.getDEFAULT_CURRENCY().equals(destCurrency)) {

            convertedAmount = sourceAmt.multiply(new BigDecimal(constants.getDEFAULT_CURRENCY_AMOUNT()).divide(new BigDecimal(rates.get(sourceCurrency)), 5, RoundingMode.CEILING));
        } else {
        	convertedAmount = calculateXchangeRate(sourceAmount, sourceCurrency, destCurrency);
        }
        logger.info(String.format(constants.getCONVERTED_AMOUNT(), sourceAmount, sourceCurrency, destCurrency));
        return convertedAmount;
    }
	
	public String getLink(String sourceCurrency, String destCurrency) {
    	
		updateRequestedCurrencyCount(sourceCurrency, destCurrency);
		logger.info(String.format(constants.getINTERACTIVE_LINK(), sourceCurrency, destCurrency));
        return String.format(constants.getCHART_URL(), sourceCurrency, destCurrency);
    }

    private void updateRequestedCurrencyCount(String sourceCurrency, String destCurrency) {
    	
    	if (!constants.getDEFAULT_CURRENCY().equals(sourceCurrency)) {
    		CurrencyXchangeCalcApplication.currencyRequested.put(sourceCurrency, CurrencyXchangeCalcApplication.currencyRequested.get(sourceCurrency)+1);
    	}
    	if (!constants.getDEFAULT_CURRENCY().equals(destCurrency)) {
    		CurrencyXchangeCalcApplication.currencyRequested.put(destCurrency, CurrencyXchangeCalcApplication.currencyRequested.get(destCurrency)+1);
    	}
	}

	private BigDecimal calculateXchangeRate(String sourceAmount, String sourceCurrency, String destCurrency) {
		
		BigDecimal convertedXchangeRate = null;
		Map<String, String> rates = CurrencyXchangeCalcApplication.ecbCurrencyList;
		if (!constants.getDEFAULT_CURRENCY().equals(sourceCurrency) && !constants.getDEFAULT_CURRENCY().equals(destCurrency)) {

			// Logic for calculating exchange rates for currencies other than EUR:
			// Calculate Source Currency to EUR reference rate eg. INR/EUR = 0.011
			// Calculate EUR to Destination Currency reference rate eg. EUR/USD = 1.20
			// Calculate the Source and Destination Currency Exchange rate eg. INR/USD = 0.011 * 1.20 = 0.0132
			BigDecimal convertedAmount = new BigDecimal(constants.getDEFAULT_CURRENCY_AMOUNT()).divide(new BigDecimal(rates.get(sourceCurrency)), 5, RoundingMode.CEILING).multiply(new BigDecimal(rates.get(destCurrency)));
			
			if (null != sourceAmount) {
				convertedXchangeRate = new BigDecimal(sourceAmount).multiply(convertedAmount);
			}
			else {
				convertedXchangeRate = convertedAmount;
			}	
		}
		return convertedXchangeRate;
	}
}
