package capital.scalable.currencyxchangecalc;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point off application, it boots the application
 */
@SpringBootApplication(scanBasePackages = { "capital.scalable" })
public class CurrencyXchangeCalcApplication {

	public static Map<String, Integer> currencyRequested = new HashMap<String, Integer>();
	public static Map<String, String> ecbCurrencyList = new HashMap<String, String>();

	public static void main(String[] args) {
		SpringApplication.run(CurrencyXchangeCalcApplication.class, args);

		/**
		 * While starting the application, check currency list is empty or not, if it is
		 * then fetch latest reference rates from ECB site.
		 */
		if (currencyRequested.isEmpty()) {
			ECBReferenceRates.getLatestCurrenciesFromEcb();
		}
		ECBReferenceRates.checkThreadState();
	}
}
