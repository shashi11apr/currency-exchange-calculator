package capital.scalable.currencyxchangecalc;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import capital.scalable.currencyxchangecalc.controller.exception.ErrorMessageKey;
import capital.scalable.currencyxchangecalc.controller.util.Constants;

/**
 * Implementation for the ECB call to fetch the latest data at 16:00:00 each day.
 */
public class ECBReferenceRates {
	
	static Timer timer = null;
	static boolean isThreadRunning = false;
	
	static {
		try {
			timer = new Timer();
		} catch (Exception e) {
			new Exception(ErrorMessageKey.INTERNAL_SERVER_ERROR.getDescription());
		}
	}
	
	/**
	 * To check the thread running status, if not set the timer as 16:01:00 everyday
	 * to call ECB website to fetch the latest reference rates.
	 */
	public static void checkThreadState(){
		
		if (!isThreadRunning) {
			
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.HOUR_OF_DAY, 16);
			calendar.set(Calendar.MINUTE, 01);
			calendar.set(Calendar.SECOND, 00);
			Date currentDate = calendar.getTime();
			timer.schedule(new ECBReferenceRates.RunEcbThread(), currentDate);
		}
	}
	
	public static class RunEcbThread extends TimerTask {
		
		@Override
		public void run() {
			isThreadRunning = true;
			ECBReferenceRates.getLatestCurrenciesFromEcb();
		}
	}
	
	/**
	 * getLatestCurrenciesFromEcb - fetch latest reference rates from ECB website @16:01:00 everyday
	 */
	
	public static Map<String, String> getLatestCurrenciesFromEcb() {

	    Constants constants = new Constants();
        Map<String, String> currencyMap = new HashMap<String, String>();
        try {
            URL obj = new URL(constants.getECB_URL());
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(response.toString())));
            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression xPathExpression = xpath.compile(constants.getCURRENCY_XPATH());
            NodeList nodeList = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node currentItem = nodeList.item(i);
                currencyMap.put(currentItem.getAttributes().getNamedItem(constants.getCURRENCY_XML_ATTRIBUTE()).getNodeValue(),
                        currentItem.getAttributes().getNamedItem(constants.getRATE_XML_ATTRIBUTE()).getNodeValue());
            }

        } catch (Exception e) {
        	new Exception(ErrorMessageKey.INTERNAL_SERVER_ERROR.getDescription());
        }
        CurrencyXchangeCalcApplication.ecbCurrencyList = currencyMap;
        for (String currency : currencyMap.keySet()) {
			CurrencyXchangeCalcApplication.currencyRequested.put(currency, 0);
		}
        return currencyMap;
    }
}
